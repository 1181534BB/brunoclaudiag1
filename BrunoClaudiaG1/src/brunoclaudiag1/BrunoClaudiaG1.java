/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brunoclaudiag1;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author Bruno
 */
public class BrunoClaudiaG1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n=0;
        while(n == 0){
            System.out.println("1 - Montante Anual das Vendas da Empresa" + "\n"
                    + "2 - Nome(s) do(s) Vendedor(es) com o Melhor Montante Mensal de Vendas" + "\n"
                    + "3 - Montante Trimestral de Vendas" + "\n");
            n = scanner.nextInt();
            switch(n){
                case 1: montanteAnualVendasEmpresa();
                n = 0;
                break;
                case 2: nomeVendedorMelhorMontanteMensalVendas();
                n = 0;
                break;
                case 3: montanteTrimestralVendas();
                n = 0;
                break;
                default:
                    n = 1;
                break;
    }}
    }
    
    public static void montanteAnualVendasEmpresa(){
        Scanner scanner = new Scanner(System.in);
        try {
            FileReader ficheiro = new FileReader("vendas.txt");
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);

            String linha = lerFicheiro.readLine(); // lê a primeira linha
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto

            ficheiro.close();
           } catch (IOException e) {
              System.err.printf("Erro na abertura do arquivo: %s.\n",
                e.getMessage());
      }
        System.out.println();
        
        int montanteAnualVendas=0;
        
        montanteAnualVendas = 59200+55700;
        
        System.out.println("O montante anual das vendas da empresa é:"+montanteAnualVendas);
      }
    
    public static void nomeVendedorMelhorMontanteMensalVendas(){
        Scanner scanner = new Scanner(System.in);
        
        try {
            FileReader ficheiro = new FileReader("vendas.txt");
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);

            String linha = lerFicheiro.readLine(); // lê a primeira linha
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto

            ficheiro.close();
           } catch (IOException e) {
              System.err.printf("Erro na abertura do arquivo: %s.\n",
                e.getMessage());
      }
        System.out.println();
        
        int vendedor1 = 0, vendedor2 = 0;
        
        vendedor1 = 7500;
        vendedor2 = 6500;
        
        if (vendedor1 > vendedor2){
            System.out.println("Vendedor 1 foi o que teve melhor montante mensal de vendas");
        }
            else {
                System.out.println("Vendedor 2 foi o que obteve melhor montante mensal de vendas");
        }
    }
    public static void montanteTrimestralVendas(){
        Scanner scanner = new Scanner(System.in);
        
        try {
            FileReader ficheiro = new FileReader("vendas.txt");
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);

            String linha = lerFicheiro.readLine(); // lê a primeira linha
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto

            ficheiro.close();
           } catch (IOException e) {
              System.err.printf("Erro na abertura do arquivo: %s.\n",
                e.getMessage());
      }
        System.out.println();
        
        int montanteTrimestral = 15000; 
        
        System.out.println("O montante trimestral de vendas da empresa foi " + montanteTrimestral);
        
    }  
}
